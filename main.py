import pycurl
import sys
import os
import re
import io

import response
from utils import normalizer, translator, set_dir, filepath


def send_request():
    c = pycurl.Curl()
    buf = io.BytesIO()

    user_id = \
        'login=bt2ycrja&passwdmd5=255c4ff8b6ff9368c1e1398544065d70&cuid=-9563325'

    # if not working - find new id code on http://sd-xbmc.info/support/ipla_identity.php

    joiner = \
        'http://getmedia.redefine.pl/action/2.0/vod/list/?category=' + sys.argv[1] + '&' + user_id

    c.setopt(c.URL, joiner)
    c.setopt(c.USERAGENT, 'mipla/23')

    c.setopt(c.WRITEFUNCTION, buf.write)
    c.perform()

    snatched = buf.getvalue()
    buf.close()
    return snatched


# wget request with filepath to utilities dir
def wget_request(title, url):
    request = 'wget -c -O "' + os.path.join(filepath(), title) + '.mp4" "' + url + '"'

    if (len(sys.argv) == 3) or re.search(sys.argv[3], title):
        os.system(request)


def download_request():
    set_dir()
    data_lists = response.download_details()

    for title, url in data_lists:
        title = translator(str(title))
        tmp = normalizer(title)

        wget_request(tmp, url)


def download_picked():
    set_dir()
    titles = []
    data_lists = response.download_details()

    print('Pick content number:')
    for title, url in data_lists:
        title = translator(str(title))
        print("[" + str(len(titles)) + "] " + title)
        titles.append(title)

    var = input('Type number to download or type "exit" to end:')

    if str(var) == 'exit':
        exit(0)
    else:
        [title, url] = data_lists[int(var)]
        title = translator(str(title))
        wget_request(normalizer(title), url)