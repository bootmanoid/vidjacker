from setuptools import setup, find_packages

setup(
    name='vidjacker',
    version='1.0',

    url='bitbucket.org/bootmanoid/vidjacker',
    author='Ania',

    packages=find_packages(),
    include_package_data=True,
    exclude_package_data={'': ['README.md']},

    scripts=[
        'vidjacker.py',
        'main.py',
        'response.py',
        'utils.py'
    ]
)
