import unicodedata
import os


def is_not_empty(string):
    new_str = str(string)
    return bool(new_str and new_str.strip())


def normalizer(word):
    new_word = unicodedata.normalize('NFKD', word).encode('ASCII', 'ignore').decode("UTF-8")
    return new_word


def translator(word):
    invalid_chars = [
        ";",
        ":",
        "!",
        "?",
        "/",
        "\\",
        "#",
        "@",
        "$",
        ")",
        "(",
        "\""
    ]
    new_word = ''.join([i for i in word if i not in invalid_chars])
    return new_word


def filepath() -> str:
    return os.path.join(os.getcwd(), 'wget', 'Downloads')


# create dir where you want to collect files
def set_dir():
    path = filepath()

    if not os.path.exists(path):
        os.makedirs(path)
