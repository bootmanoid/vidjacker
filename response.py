import xml.dom.minidom

import main

from utils import normalizer


def xml_reader() -> list or exit(0):
    snatched = main.send_request()
    dom = xml.dom.minidom.parseString(snatched)

    vods = dom.getElementsByTagName('vod')
    if vods:
        return vods
    else:
        print("Not recognized TV Show ID!")
        exit(0)


def title_info() -> list:
    vods = xml_reader()
    titles = []

    for vod in vods:
        title = normalizer(vod.getAttribute('title'))
        titles.append(title)

    print('Content info:')
    return titles


# download_details -> ([t,u],[t,u],)
def download_details():
    vod_set = xml_reader()
    data_set = []

    for vod in vod_set:
        requrl = vod.getElementsByTagName('srcreq').pop()
        data_set.append(tuple([
                            vod.getAttribute('title'),
                            requrl.getAttribute('url')]))
    return data_set