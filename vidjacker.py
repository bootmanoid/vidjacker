#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys

from response import title_info
from main import download_request, download_picked

# accepted requests
# utilities video : {script.py VIDEO_ID utilities}
# get info : {script.py VIDEO_ID info} or {script.py VIDEO_ID}

if len(sys.argv) < 2:
    print("Parameter should be equal to TV show number")
    exit(0)

elif len(sys.argv) > 2:
    if sys.argv[2] == 'download':
        print('Downloading content')
        download_request()
    elif sys.argv[2] == 'pick':
        download_picked()
    elif sys.argv[2] == 'info':
        for title in title_info():
            print(title)
    else:
        print("Request was not recognized")
        exit(0)
else:
    print("Request was not recognized")
    exit(0)
